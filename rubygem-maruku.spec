%define gem_name maruku
Name:                rubygem-%{gem_name}
Version:             0.7.2
Release:             2
Summary:             Maruku is a Markdown-superset interpreter written in Ruby
License:             MIT and BSD
URL:                 http://github.com/bhollis/maruku
Source0:             https://rubygems.org/gems/%{gem_name}-%{version}.gem
BuildRequires:       ruby(release) rubygems-devel ruby rubygem(rspec)
BuildRequires:       rubygem(rexml)
BuildArch:           noarch
%description
Maruku is a Markdown interpreter in Ruby. It features native export to HTML
and PDF (via Latex). The output is really beautiful!

%package doc
Summary:             Documentation for %{name}
Requires:            %{name} = %{version}-%{release}
BuildArch:           noarch
%description doc
Documentation for %{name}.

%prep
%setup -q -c -T
%gem_install -n %{SOURCE0}

%build

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/
mkdir -p %{buildroot}%{_bindir}
cp -a .%{_bindir}/* \
        %{buildroot}%{_bindir}/
find %{buildroot}%{gem_instdir}/bin -type f | xargs chmod a+x

%check
pushd .%{gem_instdir}
sed -i '/[Ss]imple[Cc]ov/ s/^/#/' spec/spec_helper.rb
mv spec/block_spec.rb{,.disable}
rspec spec
popd

%files
%dir %{gem_instdir}
%{_bindir}/maruku
%{_bindir}/marutex
%license %{gem_instdir}/MIT-LICENSE.txt
%{gem_instdir}/bin
%{gem_instdir}/data
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/docs
%{gem_instdir}/spec

%changelog
* Fri Feb 11 2022 xu_ping <xuping33@huawei.com> - 0.7.2-2
- Add buildrequires rubygem(rexml) to fix cannot load rexml

* Wed Aug 19 2020 geyanan <geyanan2@huawei.com> - 0.7.2-1
- package init
